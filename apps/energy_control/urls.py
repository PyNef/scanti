# -*- coding: utf-8 -*-
from django.urls import path
from apps.energy_control.views import *

app_name = 'energy_control'

urlpatterns = [
    path('units/', UnitsView.as_view(), name="units"),
]
