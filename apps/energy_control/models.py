# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from apps.accounts.choices import *


class Unit(models.Model):
    """
    Unit is the representation of a consumer unit
    """
    name = models.CharField(u'Nombre', max_length=250)
    value = models.IntegerField(u'Valor', null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.name)

    class Meta:
        ordering = ['name']
        verbose_name = u'Unidad'
        verbose_name_plural = u'Unidades'


class ConsumerUnit(models.Model):
    """
    ConsumerUnit are twelve for Radio
    """
    name = models.CharField(u'Nombre', max_length=250, null=True, blank=True)
    unit = models.ForeignKey('Unit', on_delete=models.PROTECT, related_name='+')
    radio = models.ForeignKey('Radio', on_delete=models.PROTECT, related_name='+', null=True, blank=True)
    measurer = models.ForeignKey('Measurer', on_delete=models.PROTECT, related_name='+', null=True, blank=True)
    latitud = models.CharField(u'Latitud', max_length=240, null=True, blank=True)
    longitud = models.CharField(u'Longitud', max_length=240, null=True, blank=True)
    on = models.BooleanField(default=False)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.unit.name)

    class Meta:
        ordering = ['unit']
        verbose_name = u'Unidad de consumo'
        verbose_name_plural = u'Unidades de consumos'


class Measurer(models.Model):
    """
    Measurer is a device for each consumer unit
    """
    name = models.CharField(u'Nombre', max_length=250, null=True, blank=True)
    number_code = models.CharField(u'Numero de codigo', max_length=240, null=True, blank=True)
    model = models.CharField(u'Modelo', max_length=250, null=True, blank=True)
    client = models.ForeignKey('accounts.Profile', on_delete=models.PROTECT, related_name='+', null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.unit.name)

    class Meta:
        ordering = ['number_code']
        verbose_name = u'Medidor'
        verbose_name_plural = u'Medidores'


class Radio(models.Model):
    """
    Radio
    """
    name = models.CharField(u'Nombre', max_length=250, null=True, blank=True)
    code = models.CharField(u'Código', max_length=250, null=True, blank=True)
    model = models.CharField(u'Modelo', max_length=250, null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.unit.name)

    class Meta:
        ordering = ['code']
        verbose_name = u'Medidor'
        verbose_name_plural = u'Medidores'


class ServiceOrder(models.Model):
    """
    ServiceOrder is a action for perform
    """
    subject = models.CharField(u'Asunto', max_length=250, null=True, blank=True)
    description = models.TextField(u'Descripción', null=True, blank=True)
    consumer_unit = models.ForeignKey('ConsumerUnit', on_delete=models.PROTECT, related_name='+', null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.unit.name)

    class Meta:
        ordering = ['consumer_unit']
        verbose_name = u'Order de servicio'
        verbose_name_plural = u'Ordenes de servicio'
