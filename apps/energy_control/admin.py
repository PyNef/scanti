from django.contrib import admin
from .models import *

admin.site.register(Unit)
admin.site.register(ConsumerUnit)
admin.site.register(Measurer)
admin.site.register(Radio)
admin.site.register(ServiceOrder)
