# -*- coding: utf-8 -*-
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, TemplateView

from .models import *


class UnitsView(LoginRequiredMixin, TemplateView):
    template_name = 'energy_control/unidades.html'
    def get_context_data(self, *args, **kwargs):
        context = super(UnitsView, self).get_context_data(*args, **kwargs)
        context['unidades'] = Unit.objects.all()
        return context
