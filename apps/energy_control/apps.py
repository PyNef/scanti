from django.apps import AppConfig


class EnergyControlConfig(AppConfig):
    name = 'energy_control'
