# -*- coding: utf-8 -*-
from django.urls import path
from apps.accounts.views import *

app_name = 'accounts'

urlpatterns = [
    path('signup/', SignUp.as_view(), name="signup"),
    path('users/', UsersView.as_view(), name="users"),
    path('persons/', PersonsView.as_view(), name="persons"),
]
