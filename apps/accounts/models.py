# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from apps.accounts.choices import *


class Address(models.Model):
    """
    Address for all type profiles.
    """
    address = models.CharField(u'Dirección', max_length=250)
    city = models.CharField(u'Ciudad/Colonia', max_length=160, null=True, blank=True)
    codigo_postal= models.CharField(u'Código Postal', max_length=10, null=True, blank=True)
    state = models.CharField(u'Estado', max_length=125, choices=STATE_CHOICES, null=True, blank=True)
    latitud = models.CharField(u'Latitud', max_length=240, null=True, blank=True)
    longitud = models.CharField(u'Longitud', max_length=240, null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{}'.format(self.address)

    class Meta:
        ordering = ['address']
        verbose_name = u'Dirección'
        verbose_name_plural = u'Direcciones'


class Person(models.Model):
    """
    Person is the minimum unit of the individual
    """
    number_document = models.CharField(u'Número de documento', max_length=12, unique=True)
    names = models.CharField(u'Nombres', max_length=64, blank=True)
    last_name = models.CharField(u'Apellido paterno', max_length=64, blank=True)
    second_last_name = models.CharField(u'Apellido materno', max_length=64, blank=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'{} {}, {}'.format(self.last_name, self.second_last_name, self.names)

    def get_nombre(self):
        return self.__str__()

    class Meta:
        ordering = ['last_name', 'second_last_name', 'names']
        verbose_name = u'Persona'
        verbose_name_plural = u'Personas'


class Profile(models.Model):
    """
    Profile links person to user.
    """
    user = models.OneToOneField(User, on_delete=models.PROTECT, related_name='+')
    person = models.OneToOneField('Person', on_delete=models.PROTECT, related_name='+')
    address = models.ForeignKey('Address', on_delete=models.PROTECT, related_name='+')
    type_profile = models.CharField(max_length=15, choices=TYPE_PROFILE_CHOICES, default='S')
    #audience
    is_active = models.BooleanField(default=True)
    creation_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+')
    creation_date = models.DateTimeField(auto_now=True)
    update_user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='+', null=True)
    update_date = models.DateTimeField(auto_now_add=True, null=True)

    def __str__(self):
        return u'Perfil {}'.format(self.user.username)

    def get_name_complete(self):
        names = self.person.names
        last_name = self.person.last_name
        second_last_name = self.person.second_last_name
        return u'{} {} {}'.format(names, last_name, second_last_name)

    class Meta:
        ordering = ['person']
        verbose_name = u'Profile'
        verbose_name_plural = u'Profiles'
