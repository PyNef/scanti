# -*- coding: utf-8 -*-


TYPE_PROFILE_CHOICES = (
    (u'S', u'Sin Perfil'),
    (u'C', u'Cliente'),
    (u'T', u'Trabajador'),
    (u'A', u'Administrador'),
)

STATE_CHOICES = (
		('AGS', 'Aguascalientes'),
		('BC', 'Baja California'),
		('BCS', 'Baja California Sur'),
		('CAM', 'Campeche'),
		('CHP', 'Chiapas'),
		('CHI', 'Chihuahua'),
		('DIF', 'Ciudad de México'),
		('COA', 'Coahuila'),
		('COL', 'Colima'),
		('DUR', 'Durango'),
		('GTO', 'Guanajuato'),
		('GRO', 'Guerrero'),
		('HGO', 'Hidalgo'),
		('JAL', 'Jalisco'),
		('MEX', 'México'),
		('MIC', 'Michoacán'),
		('MOR', 'Morelos'),
		('NAY', 'Nayarit'),
		('NLE', 'Nuevo León'),
		('OAX', 'Oaxaca'),
		('PUE', 'Puebla'),
		('QRO', 'Querétaro'),
		('ROO', 'Quintana Roo'),
		('SLP', 'San Luis Potosí'),
		('SIN', 'Sinaloa'),
		('SON', 'Sonora'),
		('TAB', 'Tabasco'),
		('TAM', 'Tamaulipas'),
		('TLX', 'Tlaxcala'),
		('VER', 'Veracruz'),
		('YUC', 'Yucatán'),
		('ZAC', 'Zacatecas')
)
