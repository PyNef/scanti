# -*- coding: utf-8 -*-
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView, TemplateView

from .models import *



class SignUp(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'

class UsersView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/usuarios.html'
    def get_context_data(self, *args, **kwargs):
        context = super(UsersView, self).get_context_data(*args, **kwargs)
        context['usuarios'] = User.objects.all()
        return context

class PersonsView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/personas.html'
    def get_context_data(self, *args, **kwargs):
        context = super(PersonsView, self).get_context_data(*args, **kwargs)
        context['personas'] = Person.objects.all()
        return context
