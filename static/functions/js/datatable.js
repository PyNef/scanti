$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [
          [10, 25, 50, -1],
          [10, 25, 50, "All"]
        ],
        responsive: true,
        language: {
          searchPlaceholder: "- Buscador",
          infoEmpty:      "Mostrando 0 a 0 de 0 entradas",
          infoFiltered:   "(filtrando de _MAX_ entradas totales)",
          info:           "Mostrando _START_ a _END_ de _TOTAL_ entradas",
          zeroRecords:    "No se encontraron registros coincidentes",
          infoPostFix:    "",
          decimal:        "",
          thousands:      ",",
          search:         "_INPUT_",
          lengthMenu:     "Mostrando _MENU_ entradas",
          loadingRecords: "Cargando...",
          processing:     "Procesando...",
          paginate: {
              "first":   "Primero",
              "last":    "Último",
              "next":    "Siguiente",
              "previous":"Anterior"
          },
          aria: {
               "sortAscending":  ": activar para ordenar la columna ascendente",
               "sortDescending": ": activar para ordenar la columna descendente"
           }
        }
    });
});
